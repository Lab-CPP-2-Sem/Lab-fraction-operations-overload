#pragma once
#include <iostream>

class fraction
{
private:
	int ch, zn;
	//упрощение дроби - можно сделать private-методом
	void simplify();
public:
	//в конструкторе используем значения аргументов по умолчанию
	//при этом можно написать 1 конструктор, но работающий в 3 режимах
	fraction(const int chisl = 0, const int znam = 1);
	fraction operator+ (const fraction& b) const;
	fraction operator- (const fraction& b) const;
	fraction operator* (const fraction& b) const;
	fraction operator/ (const fraction& b) const;
	fraction operator- () const;		//унарный минус
	fraction& operator+= (const fraction& b);
	fraction& operator-= (const fraction& b);
	fraction& operator*= (const fraction& b);
	fraction& operator/= (const fraction& b);
	bool operator== (const fraction& b) const;
	bool operator!= (const fraction& b) const;
	bool operator> (const fraction& b) const;
	bool operator< (const fraction& b) const;
	bool operator>= (const fraction& b) const;
	bool operator<= (const fraction& b) const;
	double getDecimal() const;

	friend fraction operator+ (const int& a, const fraction& b);
	friend fraction operator- (const int& a, const fraction& b);
	friend fraction operator* (const int& a, const fraction& b);
	friend fraction operator/ (const int& a, const fraction& b);

	friend std::ostream& operator<< (std::ostream& os, const fraction& f);
};

/*
В предложенной реализации перегрузки операций,
где первый операнд типа int не обязательно делать
дружественными. Но лучше всё же объявлять эти функции
как дружественные внутри описания класса. В этом случае
в одном месте наглядно представлены все операции и функции,
необходимые для работы с классом
*/
//fraction operator+ (const int& a, const fraction& b);
//fraction operator- (const int& a, const fraction& b);
//fraction operator* (const int& a, const fraction& b);
//fraction operator/ (const int& a, const fraction& b);